SANCTUARY CORRUPTED 2:
"SANCTUARY IN CHAOS"

Version 2.21B

by JBouley

------------------------------------------------------
GENERAL INFO:

This mod is the sequel/follow-up to my Sanctuary Corrupted mod for classic
Diablo 2.

This mod, "Sanctuary in Chaos," is for the D2 Lord of Destruction expansion, and
works with Blizzard's patch 1.09 for the game.

There is a forum for this mod. Just go to Phrozen Keep (www.d2mods.com), where I am 
one of the webmasters, and then click on the "Phrozen Forums" link. My forum is 
titled "Sanctum Arcanum." You can get there directly by going to 
http://phrozenkeep.it-point.com/forum/viewforum.php?f=14

You can also e-mail me at JeffBouley888@msn.com, but I'd really prefer you visit
my forum instead, for two reasons. First, depending on your e-mail account name
and what you put in the subject line, your message might be mistaken for spam
and deleted before I even read it. Second, problems or questions you have may be
encountered by others as well, and I'd rather than any and all players of my mod
can read the responses from me in the forum, rather than individual e-mailers
benefiting and no one else.

There are spoiler documents now that describe all of the Cube recipes and how to 
make all of the runewords. They are available in the download section for my mod 
at Phrozen Keep if you don�t care to wait until you find the necessary clues in 
the game.

-------------------------------------------------------
RECENT CHANGES/ADDITIONS TO THE MOD:

In 2.21B...

- Druid�s "Celestial Tempest" spell has been changed back to "Armageddon."I did 
make a fix for the Celestian Tempest spell so that the animations wouldn't crash 
the game and people could have a high-level cold spell for the druid. But it still 
crashes SOME people's computers. However, there is an "upgrader" kit you can use 
that will easily and simply change the skill to the "fixed" Celestial Tempest skill 
if you want to give it a try and replace Armageddon.

- New game select screen

- Four new offensive runewords added that are even more powerful than Oath of the Endless, 
previously the most powerful weapon-oriented runeword in the game.

- Lowered level requirements for the Sumr, Autn, Wint, Spri and GodsEye runes.

- Rare items are now usable in Etherseek recipes. Previously, only magic items were 
usable. Some of the more powerful outputs (set and unique items) may no longer be 
possible if the item is magical, but might be possible if the item is rare.

- Fixed an exploitable bug that allowed more sockets on set items (and maybe 
magic, rares and uniques, too) than was supposed to be the case.

In 2.21A...

- Zombies now do poison damage instead of cold damage

- Reduced chill length (and cold damage) for Ghouls and Hungry Dead

- Reduced chill length for "Shaman Followers" (Lesser Demons, Abyss Imps, Devilspawn, 
Dark Ones and Shadowkin)

- Forged Warriors have less physical damage resistance now in Normal and Nightmare 
difficulties. They also move slightly more slowly now.

- The Juggernaut (superunique Forged Warrior) in Act 1 is no longer cold-enchanted, 
but he remains extra strong and still has stone skin (plus is poison and lightning 
immune because all Forged Warriors are). He now only spawns with one or two minions 
instead of four. It should be noted that he uses an Act 2 superunique treasure drop 
instead of Act 1 drop in Normal, Act 3 superunique drop in Nightmare, and Act 4 
superunique drop in Hell...so his drops should be pretty good if you can stay in the 
battle. Remember that if you stray too far from a superunique (in terms of physical 
distance), there is a bug in D2 that may cause the treasure drop to revert to that 
of Bishibosh, which would be bad in this case, so you need to stay in the battle.

- Made exceptional and elite versions of the weapon creation recipes that usually 
take two weapons combined to make one (such as spear + brandistock = pike and 2 
war axes = giant axe). So, now you can have similar recipe flexibility/functionality 
in Nightmare and Hell difficulties. The weapons that are output will be 
high-quality.

- Added a few armor creation recipes, similar to the weapon creation recipes. There 
are four for Normal, four for Nightmare and four for Hell. The items output will 
be basic quality.

- Added a recipe to make a piece of armor or a weapon magical.

- Added a recipe to make a piece of armor or a weapon rare.

- The gems/talismans/glyphs/etc, I added to the game will drop less frequently now 
in comparision to the more traditional gems, about one-third to one-half as often 
as they did before, I think.

- The Countess and some other superunique and boss types will now drop Etherseek 
Glass sometimes.

- Added a one-handed mechanist spear AND a two-handed spear (for all you fun-loving 
amazons and barbarians out there), in normal, exceptional and elite versions 
(Mekhanik Bladestick, Pneumatic Spear and Imagenyr Razorstaff for the one-handed 
ones...and Mekhanik Bladestaff, Pneumatic Lance and Imagenyr Razorpole for the 
two-handed ones). The one-handed spear is faster and does less damage, while 
the two-handed spear is slower, does more damage and has a longer reach...but 
both types have the same inherent base powers. Hence, why their names are so 
similar.

- Added more unique items (So far, for 2.21 and 2.21a, I have added the Nail of 
Gharyghia conjurer warstaff, the Ashymihran barbed spear, the Retribution Song 
deacon mace, the Abyssoul demon heart, the Triumph's Call avenger guard, the End 
of Kings conqueror plate, the  Dire Swarm rogue crossbow, the Night Hive champion 
mail, the Blessings From Above steel skullcap, the Gehenna's Embrace spiral mail, 
the Dusty Skumpit tournament mail, the Chromatic Wreath belt, the Faces of Death 
ghastly shield, the Flametouch gloves, and the Snowqueen�s Pinch barbed spear)

- Added new sets
	- Gold Dragon's Legacy - 3 items - Gold Dragon's Fang (blade), Gold Dragon's
   Scales (spiral mail), and Gold Dragon's snout (viper mask)
	- Deadstalker Armamentarium - 4 items - Ghoulskin Coat (heavy tunic), Ghoul 
   Corpse (macabre shield), Trophy Cage (steel skullcap), and Ghoul-Hunter Fang
   (sacrificial blade)
	- Storm of the Heavens - 2 items - Cloudstrike (ceremonial spear) and 
    Thunderwreath (dragonbone breastplate)

- Added light radius to Eidolon Bears and Possessed Bears. Some players have 
reported these monsters remaining invisible/unselectable at time, so the light 
radius will help to warn you. Hirelings and summons should be able to target 
and fight these creatures even when they are "invisible."

- Added recipes to make four new socketable items called "pain spheres" that can 
be create via the Horadric Cube only.

- Druid's Armageddon spell now called "Celestial Tempest," and it does cold damage 
rather than fire damage.

- Lost Souls no longer spawn in such large groups. They also have fewer hit points 
and a smaller defensive rating. (Players relying on summons may now rejoice in the
fact that they won't spend so much time going after Lost Souls instead of real
dangers!)

- The Summoner no longer spawns as a monster in Act 3 and Act 4. Instead, Demented 
Mages are the potential monster choices, which is the way is was supposed to be to 
begin with.

-Asheara no longer spawns in Act 4.

- Added 7 recipes for treasury ingots (three of which create new recipe-only gems: 
Quicksilver Gem, Heaven's Mist and Windcrystal)

- Added seven new unique charm recipes, four of which are addition Khalim organ 
recipes.

-------------------------------------------------------
ACKNOWLEDGEMENTS:

There are so many people whose help (direct or indirect, big or small) was
instrumental in making this mod good. Here are the ones I can think of right
now:

Thanks to Phrozen Heart, iowan, Fusman, Perfect Cell, Fireheart, Thunder, Masamune's Edge, 
MPHG, AresGodofWar, Incandescent_One, Nefarius, Zhoulomcrist, Mephansteras, DigiBO, Necroboycer, 
KillBurn, Darkmage, Goldark, Khan, 54x, Adam Diggins, Max-Violence, BJ, SanSahanSan, 40 revs, 
Caimbuel, JustinSays, Yonder, etc. for general help, answers, advice, testing, suggestions and 
whatever else. I'm sure I've missed other folks who helped me...if so, I'm sorry.

Special thanks to Zhoulomcrist in particular for giving me permission to use the files from his
Zhoulomcrypt mod to make an even larger character inventory starting with version 2.10 of SiC.

On the graphics front (inventory graphics in particular), many thanks to those
whose work I either used outright (from image packs they donated to Phrozen Keep
for general use), or used as the base to build "new" images. These folks include
most prominently Stargazer, Syniq and Riparius. Also Perfect Cell who designed
my start-up screen for the game. Incandescent_One is to thank for my being able
to include the animations of several Diablo 1 monsters (Defiler, Blood Knight,
Acid Beast and Stormrider,among others), which replace a few I didn't care for
much in D2 anyway (the Quill Rats, Zakarum Priests, Baboon Demons and the Abyss
Knights/Oblivion Mages, for example).

If I forgot anyone (and I'm sure I did), please let me know and I'll add you to
the list if you really contributed to my modding somehow. ;-)
---------------------------------------------------------
INSTALLING AND USING THIS MOD:

As of the writing of this readme file, my mod is available for download only as
an MPQ file. Basically, the MPQ is the same name as the Blizzard patch you have
in your D2 folder.

If you don't have a Patch_d2.mpq file, you have not updated your game with any
of Blizzard's patches. Until you have upgraded the game using the 1.09 patch
from Blizzard, you cannot play this mod. (in fact, you cannot play any mod for
any version of D2 if you have not run some sort of patch installer on the basic
"out of the box" version of the game). You can go to Blizzard's web site and
look under the D2 stuff to find the patches, or run a search on something like
Google.

Assuming you do have the Patch_d2.mpq file for the 1.09 patch, here's what you
do:

1) Save your existing Patch_d2.mpq file to a safe place on your computer so that
you can revert back to normal D2:Lord of Destruction later if you need or want
to.

2) Take the Patch_d2.mpq file from the download of my mod and save it in the D2
folder where the original patch file was before you saved it somewhere else. In
other words, you must swap my Patch_d2.mpq for the normal one. My patch file is
modded to give you the changes in graphics and gameplay that go with my mod.

3) You can now start up D2 and begin playing my mod.

NOTE: You might be able to use an existing D2 character in my mod, but you
probably won't be able to use that character in normal umodded D2 or in anyone
else's mod after you begin playing it in my mod. Once a character enters my mod
(or anyone else's in most cases) it can only be used in that specific mod or
subsequent versions of that mod.

For this reason, you are advised to back up character files. Also, if I release
a new version of my mod, back up any character save files you have from the
earlier version of my mod in case there is some bug in my new version that might
damage one of your old characters. I�ve never had a reported case of this, as I try 
to make sure new versions will accept characters from older versions, but there are 
no guarantees in life.

Always back up files. I am not responsible for any damage to any of your
characters. also, my mod should NOT harm your computer system in any way...it's
just an altered file for D2 after all...but I will also not be held responsible
for any damage to D2 or to your computer that may occur any time after
installation of this mod. Chances are that if anything goes wrong, it has
nothing to do with my mod and is purely coincidental...I've not heard of a
single system being damaged by a D2 mod. The exception would be those rare cases
where you get a mod from a third party instead of the original source...and that
third party has hacked the mod with a virus. Even that is so rare than it's
almost unheard of. But always be cautious, and always run a virus check before
installing or unzipping anything on your computer.
---------------------------------------------------------
WARNING!!!!

If you have patch version 1.09c or 1.09d, you may find that many items with
"chance-cast skills" (spells that launch sometimes when you are hit by enemies,
when you hit enemies, or when you swing at enemies) will not work properly. This
is a Blizzard bug...I recommend that if you want to make use of such items, that
you revert back to 1.09a or 1.09b. Fact is, unless you play Bnet, there really
isn't a reason to "upgrade" to 1.09c or 1.09d anyway.

You can easily "downgrade" yourself temporarily from 1.09d to 1.09b by using the
three .dll files included in the .zip file for this mod. Simply save the
existing 1.09c or 1.09d versions of those .dll files that are currently in your
D2 folder someplace safe and then put my 1.09b versions in their place in the D2
folder. If you should want or need to go back to playing 1.09c or 1.09d, do the
reverse: put my 1.09b versions someplace safe and put the later versions back in
the folder. Whatever you do, don't delete the original .dll files in your D2
folder. You never know when you might need to go back to the most up-to-date
version of the .dll files.

Obviously, the process I've described above for swapping out .dll files is
essentially the same as the process used to swap out the patch file so that you
can play the mod to begin with.

I've been told that you only need to swap out d2game.dll, d2common.dll and
d2client.dll, but just to be on the safe side, the .zip for this mod includes
ALL the .dll files for 1.09b, just in case you run into problems and need them
to run the mod.
-----------------------------------------------------
GAME PLAY MANUAL
------------------------------------------------------

This portion of the document will serve as your primary manual for explaining
what you need to know about playing this mod. I will assume that you already
have played normal unmodded Diablo 2 extensively...if you haven't, you probably
should before you begin playing mods of the basic game.

This manual is not all-inclusive. Some things I don't explain in great details
and you can find out for yourself. Some things I may have forgotten to mention.
But if you know how to play D2 already and you read this, you will know at least
90% of what you need to know...the rest you can discover on your own. That's
part of the fun, after all.

One of the first big changes you will notice (especially if you haven't played
other mods) is that your character inventory, stash and Horadric Cube hold much
more stuff than they used to.

TABLE OF CONTENTS:

1) Character and Skills
2) NPCs and Hirelings
3) Weapons and Armor
4) Miscellaneous Items
5) Gems, Runes, Socketing, Etc.
6) Horadric Cube Recipes
7) Quests
8) Monsters

--------------------------------------------------------------------

1) CHARACTERS & SKILLS:

Characters in this mod begin with stats and equipment slightly different than
they do in normal D2. Certain stats may increase at rates that are faster or
slower than you recall, and you will notice that characters tend to move faster
than in normal D2.

The basic experience point system is as it was in D2.

Skills are altered...vastly in some cases. The biggest thing to know is that
there are no longer any skill prerequisites except for character level. In other
words, you don't have to learn one skill or a certain combination of skills
before getting a higher-level one. As long as you are high enough level to learn
a skill, you can have that skill when you have skill points available to "buy"
it.

Here are a few highlights of the character skill changes. I'm not going to go
into great detail (except for an appendix toward the end of this document to
explain the major points of the extensive changes to sorceress skills). You can
view the skill trees to get a better idea of what the skills are called and what
they are all about.

AMAZON:
Several of the amazon's bow and spear/javelin skills no longer consume arrows,
bolts, javelins, etc. Some do. You still need to equip the proper weapon to use
the skills, but n many cases, the weapon acts as a focus, and you will not use
up ammunition to cast the skills, only mana points.

The amazon has a wider range of elemental attacks. Some javelin/spear skills now
offer the chance to do fire damage...not just poison or lightning.

Two passive skills are now auras, and operate in the same way as paladin auras.

ASSASSIN:
Probably the least altered of all the classes in terms of skills, though some
small alterations have been made in terms of damage, duration, etc. But most
changes will not be very noticeable. There are some items in the game, though,
that will make her very deadly in hand-to-hand combat in conjunction with the
martial arts skills.

BARBARIAN:
The barbarian has some of the fewest skills changes of any character, mostly
because he is not as moddable as other characters.

War Cry has been beefed up to make it a more attractive skill. The skill that
used to be Whirlwind has been lessened in power somewhat (it is still powerful,
and initially costs less mana than before, but as skill level goes up, so does
mana cost...which was not the case before...and the skill has one main weakness.
If you are hit while whirling, you will stop and will be subject to attack. This
is no longer the uber-skill it once was).

Potion and magic find powers are better than they were before, and some combat
skills have been beefed up.

DRUID:
Most of the changes to druid skills are mild...graphics and/or elemental nature
of the skills have been changed. Other changes to things like shapeshifting
skills are not readily noticeable. But the druid should be more easily playable
as an elemental spell caster than he was in normal D2...for those who don't just
want to rely on summons and shapechanging. The "spirit minions" have changed in
name and appearance, but still have similar functions as they did before.

WARNING: Please note that at least two Druid spells freak out at very high
levels. When werewolf or werebear reach a level in the low- to mid-30s (34 or
35, I believe), they will crash the game when cast...this is NOT a problem
specific to my mod but an error in Blizzard's own coding for the game. I know of
no fix for this bug as of yet.

NECROMANCER:
The necro's summoned minions look a bit different, and have beefed up damage and
defenses, but are very similar in function to what they were before.

Curses are almost exactly as they were before, though some have longer or
shorter durations than they once did.

The bone and spirit tree has undergone fairly heavy changes to graphics and/or
function of skills. Most notably, the spell that originally was Teeth is now
higher level and has a much different nature. And bone spear is now a fire-based
skill that is lower level, but still pierces enemies.

PALADIN:
Many of the changes to the paladin are visual only...new aura graphics and
such...but some are quite substantive.

For one thing, Zeal can no longer be interrupted. That bug is now removed.

Also, the aura formerly known as Holy Fire, Holy Freeze and Holy Shock are now
castable skills instead of auras. They do more damage for the most part than
they did to compensate for the fact they are not always "on." But when cast, the
paladin will temporarily be bathed in magic light or have a graphic over his
head, and all monsters in the area of effect will be hit with the spell's
effects.

SORCERESS:
The most heavily altered class in terms of skills, the sorc is no longer an
elemental specialist. The changes to her skills are too many and too detailed to
go into here. Explore and enjoy (and read the appendix on sorc skills later in
this document). But here are general comments:

In addition to fire, cold and lightning attacks, the sorc has access to poison
spells and magic-damage spells.

One skill tree is mostly dedicated to combat spells. Another is dedicated
primarily to the more general spells and masteries (which affect different
skills than they once did, and are not all 30th level anymore). The last skill
tree consists of pretty powerful skills, with one catch: the sorc must be
wielding a staff or a mage sword (a new weapon) to use them. The lack of ability
to wield a one-handed weapon and shield is more than compensated for by the
increased power of these spells. However, you should remember that you can still
equip your sorc with one-handed weapon and shield...your just might have to
toggle between your two weapon/armor screens. This changes strategies for the
sorc immensely, but she is still quite deadly.

The skill formerly known as Static Field is still powerful, but has been scaled
back considerably. Cold skills are also much fewer, so don't expect to freeze
all your enemies in place.

----------------------------------------------------

2) NPCs & HIRELINGS:

You need to talk to the NPCs...often. In addition to their normal speeches, many
NPCs in town will now have additional text below the normal text. Although there
is nothing for you to hear on your computer's speakers, there are words that
will continue that you need to read for important clues regarding quests, items
and Horadric Cube recipes.

As of version 1.01, NPCs are no longer as generous about buying items back from
you. But the maximum amount that they will pay for items in Hell difficulty has
been increased.

Hirelings in some cases have increased resistances and other stats to make them
slightly hardier. But Blizzard did a pretty good job of upgrading them, so I
haven't made many changes. Hirelings in some cases have increased resistances
and other stats to make them slightly hardier. But Blizzard did a pretty good
job of upgrading them, so I haven't made many changes. The spells used by the
Iron Wolves are a bit different now...and there are a few other skill changes
here and there for mercenaries...but nothing too drastic.

---------------------------------------------------

3) WEAPONS & ARMOR:

There are many, many new types of weapons and armor in the game.

Some are class-specific. Some are geared toward a particular class but can be
used by anyone. Some items grant very specific stats and powers. Some items are
powerful but "non-magical."

For example, there are "mechanist" weapons now that always spawn with several
sockets and have built-in powers like reducing enemy AC, ignoring AC, doing
knockback, etc. They are designed to be fairly powerful in their own right but
especially so when socketed with gems, talismans, runes or jewels. They are not
intended to spawn as magical or rare items. They have no unique or set item
versions. They are "tech" weapons in a sense...though not exactly technological
(this is a swords & sorcery game, after all). In version 1.0 of the game, the
weapons would occasionally drop as magical, in violation of my intentions. As of
version 1.01 (and all subsequent versions of the mod), they can now ONLY be
created through Horadric Cube recipes (don't worry, you can now get the Cube in
Act 1 from the Smith in the Monastery)...so this bug should be eliminated.

Several new weapons and armor will boost character skills stats...though not all
of these are class-specific.

There are cloaks that can be worn like normal armor or can be used in place of a
shield if you don't care about blocking and just want to stack AC and other
stats. This essentially allows you to wear two pieces of armor at once. In some
cases, and depending upon the character type and skill choice, the combination
of stats is much preferable to wielding a shield.

In addition, as of version 1.01, there are special magical musical instruments
that can be equipped as shields. They are not very good at blocking compared to
other shields, but they offer other types of powers that may prove useful for
paladins, amazons, barbarians and druids.

Many of the new weapon and armor types can spawn as unique items. Future
versions of the mod will include even more of these...as well as unique versions
of many of Blizzard's elite weapons (which they never finished making uniques
for...only the normal and exceptional weapons).

Some special weapons and armor with very powerful abilities can only be spawned
through Cube recipes. You will have to consult NPCs and read various books and
scrolls that drop in the game to find out how to make them. There are also new
recipes for making weapons from other weapons, and for upgrading exceptional
weapons to be their elite counterparts.

Some new weapons look like gauntlets but are equipped as weapons, NOT as gloves.
They allow to be viable as a "bare handed" fighter. This is especially useful
for people who want to play "boxing barbs" or "barehanded paladins." For boxing
barbs, of course, it is important to know the weapon classes of these new items.
Most of the "melee bracer"-type weapons are mace class. Three of the
boxing/punching weapons, however, are axe class: They are the iron mitten,
dragon rake and mauler talons.

Here are a few notes on just SOME of the new armors:

Armor: 

1) Cobbled armor - Pieced together from the remains of other armors, this will
offer decent defense with low strength requirements, but has fairly low durability. 

2) Bandit armor - Offers the defensive value of studded leather, but with the
lower strength requirements and lower durability of hard leather.

3) Dragonbone breastplate - Fashioned from the carapace, scales and bone of
dragons, this offer superior protection with a slightly lower strength
requirement than normal metal breastplates.

4) Staghide pelt  - This pelt offers decent protection for characters with
extremely low strength, but is not very durable. It is, however, prized for it's
beauty and value. If you find such a hide in the wilderness, you will find it
sells for a fairly high price compared to leather armor, which is its closest
equivalent in terms of protection. 

5) Heavy Tunic - This is basically a shirt. It offers very low defense, but
might be helpful for low-strength characters and for beginning characters low on
money (esp. the sorceress and necromancer) 

6) Carved shield - Made from a special kind of semi-magical rock, these shields
offer similar defense value as Kite shields, but a better blocking rate and much
higher durability. 

7) Gryphon shield - Roughly comparable to a large shield in terms of protection,
these shields have lower durability but can inflict twice as much damage as
large shields, making them potentially useful for paladins with shield-based
offensive skills. 

8) Liege shield - The defense and durability of this shield are poor, but the
blocking rate is extremely good. 

9) Defensive rings: They increase your defense and may also have magical powers.
They are worn on the finger like any other ring. They can be socketed in some
cases, and are treated as shields for the purposes of what powers a gem,
talisman, glyph or rune will grant.

------------------------------------------------------

4) MISCELLANEOUS ITEMS:

There are tons of new miscellaneous items that serve various uses.

Axe talismans that can be turned into throwing axes (they are smaller than the
weapon before conversion, thus saving space in your inventory until needed).

Eternity Keys allow for a much larger stack than normal keys and, while
expensive, are more cost-efficient in the long run.

Special items now exist that you can buy for "gambling" purposes. These include
philosopher's stones, enigma cubes, dimensional chests, magic lots and others.
When right-clicked, they become an item that will create some other item. But
you will have no idea what you can create until you buy it. You might end up
being able to create something worth much more than the item you bought (or an
item that you might want to keep and use), or you might get junk, or anything
in-between.

Ethereal Essence is similar to the gambling items above, but offers a means to
"gamble" for gems.

New charms have been added to the game, some of which can only be created
through special Horadric Cube recipes. In general, charms tend to be larger than
they were before, partly because they can spawn with more powerful magic, and
partly because you have a much larger inventory. The larger size helps keep them
from getting too powerful.

There are many items that drop in the game that can be used to sell back to
vendors for money or that often can be part of new Horadric Cube recipes. These
include, but are not limited to: baron's jewels, spiceflower perfume, fine wine,
copper coins, ancient coins, honeystars, azureite, shaman's flags, internal
organs, bones, skulls, captured souls, magic stones, water jugs, etc. Some of
the items that are primarily intended to be sold back for money (sometimes very
substantial amounts of it) will improve at higher difficulty levels (Nightmare
and Hell).

Healing, mana and rejuvenation potions now look different and have different
names. The healing potions, for example, are no longer potions in most cases
(bread of life, shaman's bag, medicine box, etc.). The mana potions are all
green potions and have new names. The rejuvs are now the heart of glory and mind
of glory. They are enchanted organs that you "eat."

Several colored tokens now appear in the game. They are primarily used in
various special Cube recipes.

Various scrolls and books now appear in some shops and are dropped by various
monsters. They explain various recipes and give you other important information.
Others are religious or philosophical messages, or are just humorous or silly in
some cases. Many of these items consist of multiple "pages" that you "flip"
through by right-clicking the item. In all such cases, if you right-click enough
times, you will go back to the beginning. Most such items are not very long,
except for a few of the books.

Etherseek Glass is a very important item that can allow you to vastly improve
the power of weapons, armor, rings and amulets. It can also give you poor
results or destroy the items. Scrolls in the game and clues from NPCs will help
you figure out how best to use these items...but there is always an element of
risk involved.

Socketing Tools, Mechanist Weirds and Maker Forges can be used in the Horadric
Cube to add sockets to items. There can be risks involved, especially if you
socket a magic, rare, set or unique item. Specifically, you may destroy the
item's original powers if you haven't learned the right recipes to protect them.
Also, without special knowledge, you cannot exceed 3 sockets on items with
magical abilities. Bear in mind that not item can have more sockets than the
base item is intended to have (a dagger will never have 6 sockets, for example).
An appendix later in this document contains info on maximum number of sockets
for various weapons and armor and shields.

There are other useful items as well. Pay attention to clue scrolls and extra
stuff added to the end of NPC speeches in the game (most such clues are added to
end-of-quest speeches and intro speeches, but you should make sure in general to
constantly talk to NPCs).

----------------------------------------------------------

5) GEMS, RUNES, SOCKETING, ETC.

There are recipes to allow you to turn normal jewels into arcane jewels (the
equivalent of rare jewels).

All runes can be purchased in Act 5 after you rescue Anya, except for a few
"uber runes" you will learn about while playing the game.

As of version 2.0, there is a full slate of runewords in the game, some 160+
runewords. There are clues, particularly one very long string of text from
Anya's intro text, that will help you out. Please read her text on-screen
because it took me hours to put that together (and only about 10 minutes for you
to read). But for future reference after that, consult the last appendix in this
document, which has a "spoiler" with her speech in its entirety. These clues
don't give away everything but give you an idea of what runes to use in which
order. Another NPC earlier in the game gives clues about the class-specific
runewords. As of version 2.0, there are NO clues about what weapons or armor
each runeword can be applied to. Future versions may contain such clues, but for
now, you have to experiment and guess.
 
Most runes are now available for use at lower character levels than in "classic"
D2.

Many runewords that Blizzard left inactive have been activated in my mod...as of
version 1.03, there are no clues in the game to help you. By version 2.0, which
will be the next version after 1.03...there should be more active runewords and
in-game clues to help you determine what those runewords are.

As of version 1.01, there are a number of unique jewels in the mod. As with
other items in the game, there are Horadric Cube recipes that will give you a
chance of possibly turning "normal" jewels into unique versions.

Some Horadric Cube recipes will place sockets on items (though special recipes
are needed to socket magic, rare and unique items without erasing their former
powers). Very special recipes will allow up to five sockets in a magic item and
as many as 3 in a rare or unique (if the base item allows for that many).

There are more than 40 new items that can be placed into sockets, classified
generally as gems, glyphs and talismans. All of these items are considered
"gems" for purposes of the game (thus, if a Horadric Cube recipe calls for any
type of gem, you could use a glyph or talisman if you wanted).

The original gems in the game have been changed in terms of names and powers
(emeralds are now vilearth stones, rubies are dragon stones, diamonds are
soulshards, etc.). Their appearance remains the same, except that sapphires (now
moonstones) have a new appearance for the perfect variety, and the graphic
originally used for one of the lesser qualities is used for a non-socketable
stone that is simply a treasure item).

The renamed and altered "original" gems and skulls still can be upgraded in the
same way as before. Three of the same type in a Horadric Cube will yield one
stone of the next quality level. If the stone you have has the notation
"upgradeable gem" or "upgradeable talisman" it can be improved. When you reach
the highest level, the notation will be "perfect gem" or "perfect talisman."

The other gems, glyphs and talismans fall into two general categories: 
transformable and non-upgradeable. 

If one of these items has the notation "transformable," that means there are
special Cube recipes that will allow it to change into another gem, glyph or
talisman of the same group. This does NOT mean it will be more powerful. Unlike
the original gems in the game, the new gems are in groups in which each gem is
different, though similar to the others, but none are more powerful than the
others in the same group. See the appendix later in this document regarding gems
for more detailed information on this.

If a gem has the notation "non-upgradeable," that means it cannot be altered in
any way. So don't look for some recipe to change such gems, glyphs and
talismans. They might be usable as ingredients in recipes, but they themselves
cannot be changed into other gems.

Again, consult the gem appendix for more information of the "original" D2 gems
and all the new ones.

------------------------------------------------------

6) HORADRIC CUBE RECIPES:

A vast number of new recipes for the Cube now exist. Some can be gained by
finding scrolls or books in the game that explain them. Some such recipe
descriptions will tell you exactly what you need to use...some might be more
vague and clue-like. A few recipes you might just have to discover through
accident or experimentation. Some recipes have an element of risk in that they
might fail or give you unintended results sometimes. Others are consistent and reliable.

Recipes exist to create or upgrade some weapons and armor in the game.

Recipes exist to make magic and rare items...in some cases even set items or
unique items.

Many items you find, some of which can be sold to vendors for money, may also be
useful in recipes. You might be able to create healing items, mana replenishing
potions, special charms, gems, armor and weapons.

But you will have to find out for yourself exactly what you can do with the
Cube. Talk to NPCs often, and read the scrolls and books you find. Keep the
scrolls or books that you think will be most useful to you for reference later
on, or write down the recipe information on a real piece of paper or in a
computer word processing document for reference later.

You will NOT find scrolls or get clues on ALL recipes. Yes, most of the recipes
you can learn from scrolls, books or NPCs, but some you just have to find out
for yourself through experimentation and accidents. For all "unique charms"
created in the Cube, recipes are available on scrolls, but some of these recipes
might not drop until Nightmare or Hell in the mod because the charms are so
powerful and small.

Here are the names and general abilities of just a FEW of the new unique Cube-
recipe-created charms:

JEWEL CHARMS (all 1-by-1 in size):

>>Wine Prism (red): Cast lvl 5 Confuse when hit (9% chance); cast lvl 3 Attract
upon hitting (9% chance), Cast Taunting Presence aura at will

>> Seafoam Lens (blue): Cast lvl 3 Tornado at will; Fire resist 12%-18%;
Knockback

>> Sepia Shard (yellow): Cast lvl 5 Meditation aura at will; Hit points 28;
Strength 25

>> Alpine Facet (green): 20% faster hit recovery; 50% faster run/walk; Resist
cold 11-15%; Cast lvl 10 Leap at will

>> MilkGlass (white/clear): Resist lightning 15-21%; Resist magic 32-40%; 8 min
damage; 20 max damage; 12-15% chance of deadly strike

>> Lilac Sun (purple): 65 to attack rating vs. undead; 80 to attack rating vs.
demons; Cast lvl 3 Sanctuary aura at will; Cast lvl 6 Holy Bolt when hit (10%
chance)

ARCANE CHARMS (all are 2-by-2 in size):

>> Razor Spiral: Ignore target's AC; Cast lvl 3 Dragonfangs at will; Attacker
takes damage of 10; Cast lvl 5 Cyclone Armor at will; Cast lvl 4 Iron Maiden
when hit (15% chance)

>> Seeds of Ares: Cast the following summon spells at will - lvl 12 Raise
Skeleton; lvl 6 Raise Skeletal Mage; lvl 4 Summon Dire Wolf; lvl 4 Summon
Banshee; lvl 4 Solar Creeper

>> DragonScale Torus: Cast lvl 2 Armageddon on hitting (5% chance); Cast lvl 4
Fissure when hit (15% chance); Cast lvl 15 Leap at will; Cast lvl 11 Dragon
Talon at will; Cast lvl 9 Dragon Tail at will

---------------------------------------------------

7) QUESTS:

For the most part, quests are as they were in the original game.

The jade figurine/golden bird quest has been altered very slightly...but the
results are essentially the same.

The quests for Khalim's organs and Khalim's Flail do NOT have to played out as
intended. You CAN do the quests as originally intended...but there IS a way to
make Khalim's Will to destroy the Compelling Orb...without having to use the
organs. There are other uses for Khalim's body parts which can have more long-
term value to your character. Information from NPCs and from at least one other
source will help guide you in making your decision about which route to take.

Finally, there is an extra, "hidden" quest. It is not a quest in the traditional
sense, but there are clues from Gheed and Fara that will point you toward
finding an item that has very important information for you.

----------------------------------------------------

8) MONSTERS:

There are a multitude of new enemies in the game. Many of the unique monsters in
the game (the "mini-bosses" for lack of a better term...like Corpsefire,
Rakanishu, Treehead Woodfist, etc.) now have new names, different powers and
different appearances.

Diablo's "seal bosses" are NOT the same as in the original game. And most of
Baal's minions are vastly different than before.

In general, there are very different "normal" monsters as well. There are fire
elemental monsters, blood golem-like monsters, eerie-looking extraplanar
creatures (Planeshifters, Cacodemons and Pandamonium Hunters) with powerful
spectral hits and telekinesis, possessed wolves and bears, astral wolves,
demonic werebears and werewolves with stunning hits, metallic warriors,
leviathans (spawn of Duriel), incubi, invisible demons that you cannot hit with
most attacks (thankfully your hirelings and minions can), and a slew of others.

Some of the original monsters have been somewhat altered. Some of the more
powerful species of quill rats, for example, launch fiery missiles (as well as
having been changed into a different-looking monster). Some monsters do
elemental damage when they hit you in addition to normal damage. Many monsters
have increased resistances. Most have more hit points and are significantly
faster than before.

In other words, be careful out there. ;-)

Here are brief notes on most of the new monsters:

1) Planeshifter/Cacodemon/Pandemonium Hunter - These three monsters comprise a
new species of extraplanar enemies. They look weird, and not "all there." They
have a Telekinesis-like power with damage and knockback, and a spectral attack
in hand-to-hand combat 

2) Reaver - Half-decayed, crumbling liches that were once lightning-oriented
sorcs. They retain a stunning Thunder strike with which to stun you, but have no
other notable skills. 

3) Chaos Warriors - Zombie-like thugs, basically (they look like Griswold) 

4) Troll - Higher-than-normal damage regeneration, and a poison attack with
their hammers 

5) Iron Brutes & Forged Warriors - These two types of iron golem-like monsters
are usually found near each other. The brutes will surround and beat on you. The
Forged Warriors are smarter and deadlier, with an electrical attack to add to
the fun. 

6) Mage Hunter - These cousins to the Sand Raiders will drain your mana as they
slice you to ribbons 

7) Saurian Magus - The Dragon Lords you may remember if you played my Sanctuary
Corrupted Mod for classic D2 are gone...but their levitating cousins are here to
replace them. Unlike the Dragon Lords, you won't get spells cast at you from
these partial Diablo clones...but you will be stunned if they hit you. 

8) Leviathan - Spawn of Duriel...these creatures are just like him except for
the lack of a Freeze aura. 

9) Basilisk - A flying, serpentine creature whose bites inflict elemental
damage. 

10) Ice Basilisk - A cousin to the basilisk, with a charging, freezing attack. 

11) Demon Rooster - These monsters are only found in the Cow Level. They aren't
roosters exactly...but they looked so much like giant, mutant, demonic black
chickens that I had to add them in with the Hell Bovines.

12) Eidolon Bear - Interdimensional bear that shifts in and out of this plane as
it attacks you.

13) "Ghost dogs" - Wolves that are of the same nature as the eidolon bears...but
they come in three types: Phase Wolves, Wolvren, and Astral Hounds.

14) Lost Souls - Harmless balls of reddish light; they can be killed but can do
no harm.

15) Sin Monkeys - These impish creatures are fast and numerous, but often
consume themselves in a fiery death before you can kill them.

16) Elemental Scourges - Demonic clouds of energy that have a spectral hit with
their tendrils, and a fiery bolt of energy with which to burn you.

17) Eidolons - Smoky, wraith-like creatures with a highly chilling touch.

18) Gorgon - A half-insect, half-reptilian creature that flies without benefit
of wings and can render you almost immobile when they strike you. 

19) Chimera - A chitinous, large creature with a vicious stunning charge. This
is essential Baal's Minion of Destruction, now spawnable outside of the final
battle with Baal.

20) Wolf Striders - Werewolves with stunning hit.

21) Demon Bears - Humanoid, hell-spawned bears that can stun you, even if they
cannot quite reach you with their claws.

22) Giant Mantis - Large, bipedal insect-like monsters that are highly resistant
to several kinds of attacks.

23) Thorn Dogs - These dog-like creatures spit deadly projectiles, including
dart-like barbs, fiery quills, hellfire spit and electrified thorns.

24) Djinni - These devilish mystical creatures will summon the power of the
skies to destroy you. They are said to be allied with Mephisto and to be
secretly leading the corrupted Zakarum Order.

25) Claw Vipers - The old claw vipers are gone (see the rhino demon entry next).
The creatures you one knew as Sand Leapers in D2 are now referred to as claw
vipers, they have a poisoning attack, and they appear mostly in the Claw Viper
Temple, along with Claw Viper Magi and some other "friends."

26) "Rhino Demons" - With names like Saberheads, Tomb Guardians, etc., these
monsters replace the original claw viper animations in the game. They have
similar attacks, such as charging and freezing you, but they look much
different.

27) Swarms - The bug swarms are much different than before. They have new names,
some of them inflict new kinds of damage (poison or fire) instead of draining
stamina, and ALL of them are immune to physical damage. It never made sense
anyway that you could kill a whole swarm with a sword.

28) World Corruptors - These plant-like monsters poison the air and ground to
make you sick, they can knock you back with befouled blasts of air, and they can
strike you with venomous tentacle-like vines from the ground.

29) Fire Spirits - The Flamchylde and the Sunbeast are two creatures you will
meet. Immune to fire and pretty resistant to weapons, they can be quite
dangerous.

30) Plaguefire Spirits - These monsters look like the fire spirits, at least
until they develop a sickly green glow in their bodies and periodically vanish
from sight. They have a vicious poison strike.

31) Phase Hunters - Looking like Death Maulers from the frigid lands of the
barbarians, but without the underground tentacle attack, these creatures vanish
in and out of sight and can be quite dangerous to your health.

32) Lizard Beasts - The Demoni and Salamanz are cousins to the "frog demons" you
find in and around the jungles of Kurast. They don't spit poison, but they can
still pose quite a threat to you.

--------------------------------------------------------
APPENDIX 1 - Number of Sockets allowed:


MAXIMUM SOCKETS ALLOWED FOR WEAPONS, ARMOR AND SHIELDS:

SANCTUARY CORRUPTED 2:
"SANCTUARY IN CHAOS"

by JBouley

------------------------------------------------------
MAXIMUM SOCKETS ALLOWED FOR WEAPONS, ARMOR AND SHIELDS:
-------------------------------------------------------

1) WEAPONS:

Item			Max Gem Sockets

Hand Axe			2
Iron Mitten			4
Double Axe			5
Battle Bracers		6
War Axe			6
Large Axe			4
Broad Axe 			5
Battle Axe			5
Great Axe			6
Giant Axe			6
Wand				1
Yew Wand			1
Bone Wand			2
Grim Wand			2
Heavy Rod			2
Scepter			3
Grand Scepter		3
War Scepter			5
Spiked Club			3
Mace				3
Morning Star		3
Flail				5
War Hammer			4
Maul				6
Great Maul			6
Short Sword			2
Hooked Blade		2
Scimitar			2
Holy Sword			2
Crystal Sword		6
Broad Sword			4
Long Sword 			4
War Sword			3
Two-Handed Sword		3
Claymore			4
Mage Sword			4
Bastard Sword		4
Flamberge			5
Great Sword			6
Dagger			1
Dirk				1
Sacrificial Blade		3
Blade				2
Phoenix Charm		0
Charged Orb			0	
Arctic Rod			0
Balanced Axe		0
Javelin			0
Thunder Javelin		0
Short Spear			0
Glaive			0
Throwing Spear		0
Spear				3
Trident			4
Brandistock			5
Spetum			6
Pike				6
Bardiche			3
Voulge			4
Scythe			5
Poleaxe			5
Halberd			6
War Scythe			6
Short Staff			2
Long Staff			3
Gnarled Staff		4
Battle Staff		4
War Staff			6
Short Bow			3
Hunter's Bow		4
Long Bow			5
Battle Bow			4
Skirmish Bow		5
Long Battle Bow		6
War Bow			5
Assault Bow			6
Light Crossbow		3
Crossbow			4
Heavy Crossbow		6
Repeating Crossbow	5
Viper Gem			0
Blazing Talisman		0
Venom Stone			0
Demonsfire Skull		0
Contact Poison		0
Sorcerer's Stone		0
Decoy Gidbinn		0
Gidbinn			0
Wirt's Leg			3
Horadric Malus		0
Hellforge Hammer		5
Horadric Staff		0
Staff of the Kings	0
Meele Axe			2
War Bracers			6
Twin Axe			5
Military Pick		6
Naga				6
Military Axe		4
Bearded Axe 		5
Tabar				5
Gothic Axe			6
Ancient Axe			6
Burnt Wand			1
Petrified Wand		2
Tomb Wand			2
Grave Wand			2
Enchanter's Rod		2
Rune Scepter		2
Holy Water Sprinkler	3
Divine Scepter		5
Barbed Club			3
Flanged Mace		2
Jagged Star			3
Scourge			5
Battle Hammer		4
War Club			6
Martel de Fer		6
Gladius			2
Assassin Blade		2
Shamshir			2
Angelic Sword		2
Dimensional Blade		6
Battle Sword		4
Rune Sword			4
Ancient Sword		3
Espadon			3
Dacian Falx			4
Magus Sword			4
Gothic Sword		4
Zweihander			5
Executioner Sword		6
Poignard			1
Stilleto			1
Kris				3
Crystal Blade		2
Archmage Wand		0
Dragon Talons		0
Dragonlord Rod		0
Hurling Axe			0
War Javelin			0
Tempest Glaive		0
Simbilan			0
Spiculum			0
Harpoon			0
War Spear			3
War Fork			4
Barbed Pike			5
Yari				6
Lance				6
Lochaber Axe		3
Bill				4
Battle Scythe		5
Partizan			5
Bec-de-Corbin		6
Grim Scythe			6
Jo Staff			2
Quarterstaff		3
Cedar Staff			4
Gothic Staff		4
Rune Staff			6
Edge Bow			3
Razor Bow			4
Cedar Bow			5
Double Bow			4
Short Siege Bow		5
Long Siege Bow		6
Rune Bow			5
Gothic Bow			6
Arbalest			3
Siege Crossbow		4
Ballista			6
Chu-Ko-Nu			5
Khalim's Flail		3
Khalim's Will		3	
Katar				2
Wrist Blade			2
Hatchet Hands		2
Cestus			2
Claws				3
Blade Talons		3
Scissors Katar		3
Quhab				3
Wrist Spike			2
Fascia			2
Hand Scythe			2
Greater Claws		3
Greater Talons		3
Scissors Quhab		3
Suwayyah			3
Wrist Sword			3
War Fist			2
Battle Cestus		2
Feral Claws			3
Runic Talons		3
Scissors Suwayyah		3
Skirmish Axe		2
Conquerer Fists		6
Ettin Axe			5
War Spike			6
Berserker Axe		6
Feral Axe			4
Silvren Axe			5
Decapitator			5
Champion Axe		6
Glorious Axe		6
Polished Wand		2
Ghost Wand			2
Lich Wand			2
Unearthed Wand		2
Truncheon			2
Mighty Scepter		3
Seraph Rod			3
Caduceus			5
Tyrant Club			3
Reinforced Mace		3
Devil Star			3
Flayer			5
Legendary Mallet		4
Ogre Maul			6
Thunder Maul		6
Falcata			2
Ataghan			2
Elegant Blade		2
Hydra Edge			2
Phase Blade			6
Conquest Sword		4
Cryptic Sword		4
Mythical Sword		3
Legend Sword		3
Highland Blade		4
Balrog Mageblade		4
Champion Sword		4
Colossus Sword		5
Colossus Blade		6
Bone Knife			1
Mithral Point		1
Fanged Bloodclaw		3
Legend Spike		2
Magelord Wand		0	
Drake Talons		0
Conflagration Rod		0
Winged Axe			0
Hyperion Javelin		0
Stygian Stormfang		0
Balrog Spear		0
Ghost Glaive		0
Winged Harpoon		0
Hyperion Spear		3
Stygian Pike		4
Siege Lance			5
Ghost Spear			6
War Pike			6
Ogre Axe			3
Colossus Voulge		4
Devil Thresher		5
Cryptic Axe			5
Great Poleaxe		6
Giant Thresher		6
Harbinger Rod		2
Stone Staff			3
Elder Staff			4
Highlord Staff		4
Archon Staff		6
Spider Bow			3
Blade Bow			4
Shadow Bow			5
Great Bow			4
Diamond Bow			5
Crusader Bow		6
Ward Bow			5
Hydra Bow			6
Pellet Bow			3
Gorgon Crossbow		4
Colossus Crossbow		6
Demon Crossbow		5
Eagle Orb			2
Sacred Globe		2
Smoked Sphere		2
Clasped Orb			2
Dragon Stone		2
Stag Bow			5
Reflex Bow			6
Maiden Spear		6
Maiden Pike			6
Maiden Javelin		0	
Glowing Orb			2
Crystalline Globe		2
Cloudy Sphere		2
Sparkling Ball		2
Swirling Crystal		2
Ashwood Bow			5
Ceremonial Bow		6
Ceremonial Spear		6
Ceremonial Pike		6
Ceremonial Javelin	0
Heavenly Stone		2
Eldritch Orb		2
Demon Heart			2
Vortex Orb			2
Dimensional Shard		2
Matriarchal Bow		5
Grand Matron Bow		6
Matriarchal Spear		6
Matriarchal Pike		6
Matriarchal Javelin	0
Totem Flail			6
Eldritch Scourge		6
Patriarch Staff		6
Lawstaff			6
Melee Bracers		4
Dragon Rake			4
Chimera			5
Basilisk			5
Spirit Harp			5
Glory Arc			5
Bladed Staff		6
Razor Staff			6
Dueling Spear		4
Barbed Spear		4
Dragonbone Longblade	3
Dragonbone Moonblade	3
Mauler Talons		4
Leviathan Sling		5
Avenger Crescent		5
Conjurer Warstaff		6
Huntmaster Spear		4
Dragonspine Scimitar	3
Pastorship Staff		6
Ghoul Scourge		6
Katana			3
Moon Blade			3
Ninjitsu Sword		3
Throwing Stars		0	 
Shurikin	 		0
Deadly Stars	 	0
Astral Razor		5
Pandemonium Blade		5
Interplanar Sword		5
Sling				3
Eternity Blade		3
Spirit Shards		2
Ogre Jaw			2
Leviathan Jaw		2
Dragon Jaw			2
Panther Claws		2
Wendigo Claws		3
Malebrogia Talons		4
Kraken Jaw Blade		4
Dragonsfang Sword		5
Titan Jaw Falchion	6
Harbinger Mace		3
Weirding Mace		3
Huntress Rod		3
Throwing Hammer		2
Hurling Maul		2
Balanced Maul		2
Nexus Sword			2
Rift Sword			2
Energy Blade		2
Nexus Orb			4
Warp Orb			4
Locus Orb			4
Tomahawk			3
Tribal Axe			3
Chieftan Hatchet		3
Disciple Mace		3
Deacon Mace			3
Bishop Mace			3
Mekhanik Warblade		3
Tekhnyk Longrazor		4
Inertial Greatrod		3
Kinetic Strikestaff		4
Forgemaster Granbludgeon	3
Vulkhan Warstriker		4

--------------------------------------------------------
2) ARMOR, SHIELDS, HELMS, ETC.:

Item			Max Gem Sockets

Mesh Headband	5
Charmed Circlet	5
Helm			2
Full Helm		2
Great Helm		3
Crown			3
Mask			3
Quilted Armor		2
Leather Armor		2
Hard Leather Armor	2
Studded Leather 	2
Ring Mail		3
Scale Mail		2
Chain Mail		2
Breast Plate		3
Splint Mail		2
Spirit Armor		0
Field Plate		2
Gothic Plate		4
Full Plate Mail	4
Ancient Armor	4
Mage Plate		3
Buckler		1
Small Shield		2
Large Shield		3
Kite Shield		3
Tower Shield		3
Gothic Shield		4
Leather Gloves	0
Heavy Gloves		0
Chain Gloves		0
Light Gauntlets	0
Gauntlets		0
Boots			0
Heavy Boots		0
Chain Boots		0
Light Plated Boots	0
Greaves		0
Narrow Belt		0
Light Belt		0
Belt			0
Heavy Belt		0
Plated Belt		0
Bone Helm		2
Bone Shield		3
Spiked Shield		4
Dragonskn Headwrap	5
Mage Circlet		5
Runic Helm		2
War Helm		2
Chaos Helm		3
Grand Crown		3
Death Mask		3
Warding Cloak	4
Serpentskin Armor	2
Demonhide Armor	2
Trellised Armor	2
Linked Mail		3
Tigulated Mail		2
Mesh Armor		2
Cuirass		2
Russet Armor		2
Dimensional Armor	2
Sharktooth Armor	2
Embossed Plate	4
Chaos Armor		4
Ornate Armor		4
Magus Coat		3
Defender		1
Round Shield		2
Scutum		3
Dragon Shield		3
Pavise			3
Ancient Shield		3
Demonhide Gloves	0
Sharkskin Gloves	0
Heavy Bracers		0
Battle Gauntlets	0
War Gauntlets		0
Demonhide Boots	0
Sharkskin Boots	0
Mesh Boots		0
Battle Boots		0
War Boots		0
Demonhide Sash	0
Sharkskin Belt		0
Mesh Belt		0
Battle Belt		0
War Belt		0
Grim Helm		2
Grim Shield		2
Barbed Shield		2
Wolf Head		3
Hawk Helm		3
Antlers			3
Falcon Mask		3
Spirit Mask		3
Jawbone Cap		3
Fanged Helm		3
Horned Helm		3
Assault Helmet	3
Avenger Guard	3
Targe			4
Rondache		4
Heraldic Shield	4
Aerin Shield		4
Crown Shield		4
Preserved Head	2
Zombie Head		2
Unraveller Head	2
Gargoyle Head	2
Demon Head		2
Circlet			2
Coronet		2
Tiara			3
Diadem		3
Gorgnflsh Headband	5
Arcane Circlet		5
Hydraskull		2
Giant Conch		2
Spired Helm		3
Corona			3
Demonhead		3
Dusk Shroud		6
Wyrmhide		4
Scarab Husk		4
Wire Fleece		4
Diamond Mail		4
Loricated Mail		4
Boneweave		4
Great Hauberk		4
Hellforged Mail	4
Balrog Coat		2
Kraken Shell		4
Lacquered Plate	4
Shadow Plate		4
Sacred Armor		4
Archon Plate		4
Heater			2
Luna			2
Hyperion		3
Monarch		4
Aegis			4
Ward			4
Bramble Mitts		0
Vampirebone Gloves	0
Vambraces		0
Crusader Gauntlets	0
Ogre Gauntlets	0
Wyrmhide Boots	0
Scarabshell Boots	0
Boneweave Boots	0
Mirrored Boots	0
Myrmidon Greaves	0
Spiderweb Sash	0
Vampirefang Belt	0
Mithril Coil		0
Troll Belt		0
Colossus Girdle	0
Bone Visage		0
Troll Nest		3
Blade Barrier		3
Alpha Helm		3
Griffon Headress	3
Hunter's Guise		3
Sacred Feathers	3
Totemic Mask		3
Jawbone Visor		3
Lion Helm		3
Rage Mask		3
Savage Helmet	3
Slayer Guard		3
Akaran Targe		4
Akaran Rondache	4
Protector Shield	4
Gilded Shield		4
Royal Shield		4
Mummified Trophy	2
Fetish Trophy		2
Sexton Trophy		2
Cantor Trophy		2
Heirophant Trophy	2
Blood Spirit		3
Sun Spirit		3
Earth Spirit		3
Sky Spirit		3
Dream Spirit		3
Carnage Helm		3
Fury Visor		3
Destroyer Helm	3
Conquerer Crown	3
Guardian Crown	3
Sacred Targe		4
Sacred Rondache	4
Ancient Shield		4
Zakarum Shield	4
Vortex Shield		4
Minion Skull		2
Hellspawn Skull	2
Overseer Skull		2
Succubas Skull	2
Bloodlord Skull	2
Cobbled Armor	0
Salvaged Armor	0
Bandit Armor		3
Brigand Armor	4
Battle Plate		3
Templar Armor	4
Dragonbone Brstplate	2
Wyrmshell Brstplate	3
Staghide Pelt		0
Heavy Tunic		3
Carved Shield		3
Stone Shield		3
Gryphon Shield	4
Chimera Shield	5
Liege Shield		3
Festival Shield		3
Protective Ring	1
Defender Ring		1
Shielding Ring	1
Deflecting Ring	1
Guardian Ring		1
Sentinel Ring		1
Warding Ring		1
Armor Ring		1
War Remnants		0
Mercenary Armor	4
Siege Coat		4
Titan Sternum		3
Ornate Shield		3
Manticore Shield	5
Ascension Shield	3
Shell Ring		1
Repelling Ring	1
Overseer Ring		1
Displacement Ring	1
Ki-Rin Pelt		3
Sphinx Pelt		3
Regal Tunic		0
Dragonsilk Tunic	0
Brute Armor		3
Warlord Armor	3
Conqueror Plate	3
Slayer Cape		4
Magehunter Cape	4
Seeker Cape		4
Assassin Cloak	4
Shadow Cloak		4
Mageslayer Cloak	4
Twilight Shroud	4
Nemesis Shroud	4
Vengeance Shroud	4
Assassin Armor	2
Executioner Leathers	2
Judgment Armor	2
Sisterhood Greaves	0
Matre Boots		0
Furie Greaves		0
Talon Boots		0
Dragonclaw Boots	0
Slayer Greaves	0
Claw Armor		3
Razor Carapace	3
Massacre Brace	3
Earth Armor		4
Gaia Coat		5
Unity Mantle		6
Talismanic Mantle	5
Totemic Mantle	6
Archmagus Mantle	6
Witchclan Visor	3
Enchantress Cap	3
Incantatrix Helm	3
Mage Hands		0
Wizardskin Gauntlets	0
Viseraj Hands		0
Charm Drum		1
Magic Flute		1
Enchanted Harp	1
Spirit Music Horn	1
Mystic Violin		1
Arcane Singlestring	1
Leather Cap		2
Cowl			2
Ogrehide Skullcap	2
Macabre Shield	4
Ghastly Shield		4
Grotesque Shield	5
Squire Helm		2
Demihelm		2
Steel Skullcap		2

---------------------------------------
APPENDIX 2: Gems and other socketable items

GEM & TALISMAN DETAILS:

I freely admit that not all of the stats below may be 100% accurate. I had to
make many last-minute changes, and I have not had time to compare everything
here with the gems in the game. Sorry, but this will get you most of the way to
understanding what the gems will do. 

I'm not a big believer in telling players EVERYTHING about the mod anyway...so
expect to discover that some stuff might not be what you expect, and some things
might be better or worse than what I've indicated in the readme files.
-----------------------------------------------------

A) UPGRADEABLE GEMS

These are essentially modified versions of the original gems/skulls. Three of
the same type in the Cube will transform into one gem/talisman of the next
highest level in that group. There is also one added type with only three levels
of quality that was never in D2 at all under any incarnation, and which has been
added since version 2.0; the gems in this new group are called Fire Opals. 

>> Amethysts 
Types (lowest to highest):
* Charmed Amethyst, Enchanted Amethyst, Runic Amethyst, Arcane Amethyst, Royal
Amethyst 
Powers:
* Weapons - Bonus to attack rating, lightning damage
* Armor/helms - Find more gold, Resist all
* Shield - Increased durability, Mana per kill 

>> Sunstones
(formerly topaz) 
Types (lowest to highest):
* Corrupted Sunstone, Tainted Sunstone, Dim Sunstone, Bright Sunstone, Pure
Sunstone 
Powers:
* Weapons - Hit blinds target (on Dim Sunstone and higher types only), Bonus to
min/max weapon damage
* Armor/helms - half freeze duration (on Dim Sunstone and higher types only),
Increased defense vs. melee, Bonus to Hit Points
* Shield - Attacker takes damage, Bonus to Defense, Increased blocking speed 

>> Moonstones
(formerly sapphire) 
Types (lowest to highest):
* Shattered Moonstone, Damaged Moonstone, Carved Moonstone, Faceted Moonstone,
True Moonstone 
Powers:
* Weapons - Mana steal, Chance of deadly strike
* Armor/helms - Bonus to mana, Better chance to find magic items
* Shield - Absorb poison damage, Bonus to defense vs. missile attacks
 
>> VileEarth Stones
(formerly emerald) 
Types (lowest to highest):
* VileEarth Fragment, VileEarth Shard, VileEarth Stone, VileEarth Jewel,
VileEarth Gem 
Powers:
* Weapons - Poison damage
* Armor/helms - Reduce poison length, Chance of open wounds
* Shield - Resist poison, Reduced requirements to equip 

>> Dragonstones
(formerly ruby) 
Types (lowest to highest):
* Dragon's Eye, Dragon's Blood, Dragon's Breath, Dragon's Heart, Dragon's Soul
Powers:
* Weapons - Fire damage, Bonus to strength
* Armor/helms - Absorb fire damage, Bonus to stamina regeneration
* Shield - Resist fire, Light radius 

>> Soulshards
(formerly diamond) 
Types (lowest to highest):
* Flawed Soulshard, Simple Soulshard, Lesser Soulshard, Greater Soulshard, Regal
Soulshard 
Powers:
* Weapons - Increased damage to undead, Increased damage to demons
* Armor/helms - Absorb lightning damage, Faster run/walk
* Shield - Resist lightning, Bonus to dexterity 

>> Pharoanic Talismans
(formerly skull) 
Types (lowest to highest):
* Jade Torus, Ankh, Blood Sphere, Scarab, Pharoah's Scarab 
Powers:
* Weapons - Life steal, Cold damage
* Armor/helms - Absorb cold damage, Chance to make monster flee when hit
* Shield - Regenerate health, Resist cold 

>> Fire Opals
(didn't exist in normal D2; added to SiC as of version 2.0) 
Types (lowest to highest):
* Raw Fire Opal, Polished Fire Opal, Faceted Fire Opal (unlike the other
upgradeable gems, there are only three levels of quality) 
Powers:
* Weapons - Adds magic damage, increases Energy
* Armor/helms - Absorb magic damage (not spells in general; just those with the
elemental type known as "magic" and a percentage of damage taken by you is added
to your mana
* Shield - Resist magic (the elemental type known as magic, NOT spells in
general) and bonus to mana level

-------------------------------------
B) TRANSFORMABLE GEMS & TRANSFORMABLE TALISMANS

These items can changed into another gem/talisman in the same category. This is
not an upgrade. Each gem/talisman in a category is more or less considered equal
in power compared to the others. (Droppable scrolls in the game and clues from
NPCs will tell you how to make the changes) 

CATEGORY 1 - Carbuncles 
(4 types) 

>> Elemental Carbuncles 

Types:
* Spectral Carbuncle, Magenta Carbuncle, Fiery Carbuncle 

Powers:
* Weapons - Fire damage, Cold damage, Lightning damage
* Armor/helms - Increased gold from monsters, Increased chance to find magic items
* Shield - Resist fire, Resist cold,Resist lightning 

Notes:
All three elemental carbuncles have all of the bonuses noted above. However,
they differ in how much power goes into any given aspect. For damage and
resistance bonuses, the spectral carbuncle is strongest in lightning and weakest
in fire; the magenta carbuncle is strongest in cold and weakest in lightning;
the fiery carbuncle is strongest in fire and weakest in cold. For the gold and
magic find functions, spectral favors magic find; magenta favors gold; and fiery
has similar values for gold and magic. 

>> Astral Carbuncle 

Powers:

Each astral carbuncle grants bonuses in two of three speed categories: increased
swing speed, increased blocking speed, and increased hit recovery. Depending on
what kind of item you socket the carbuncle in, one value is high (fastest) while
the other value is "low" (fast). "Faster" (which is in-between) is not an option
when socketing astral carbuncles. 
* Weapons - Fastest swing, fast block
* Armor/helms - Fastest hit recovery, fast swing
* Shield - Fastest blocking, fast hit recovery 


CATEGORY 2 - Magestones 
(4 types) 

Types:
* Auburn Magestone, Crimson Magestone, Royal Magestone, Shadowy Magestone 

Powers: 

>> Auburn Magestone 
* Weapons - Increased mana, Increased strength
* Armor/helms - Increased chance to find magic items, Increased defense
* Shield - Bonus to max fire resistance, Bonus to hit points 

>> Crimson Magestone 
* Weapons - Percentage increase to mana, Increase to dexterity
* Armor/helms - Attacker takes damage, Regenerate health
* Shield - Faster blocking speed, Damage taken by hero(non-magical/non-
elemental) is reduced 

>> Royal Magestone 
* Weapons - Regenerate mana faster, Bonus to vitality
* Armor/helms - Fast cast rate, Bonus to defense vs. missile attacks
* Shield - Percent of damage taken by hero goes to refill mana, Fast run/walk 

>> Shadowy Magestone 
* Weapons - Increased mana (based on character's level), Bonus to energy
* Armor/helms - Faster casting rate, Fast hit recovery
* Shield - Attacker takes lightning damage, Resist all 


CATEGORY 3 - Ancient Glyphs 
(3 types) 

Types:
* Zodiac Glyph, Kraken Glyph, Crown Glyph 

Powers: 

>> Zodiac Glyph 
* Weapons - Maximum damage +12, 12% chance of deadly strike
* Armor/helms - Defense increased by 12%, +12 to mana, +12 to health
* Shield - Stamina +12, Max mana increased by 12%, +12 to defense 

>> Kraken Glyph 
* Weapons - Faster swing speed, Bonus to maximum damage, Bonus to mana
* Armor/helms - Life steal, Mana steal
* Shield - Damage (magical/elemental) taken by hero is reduced, Increased gold
from monsters 

>> Crown Glyph 
* Weapons - Bonus to defense vs. melee, Increased durability
* Armor/helms - Fast hit recovery, Fast casing rate, Bonus to vitality
* Shield - Fast blocking speed, Increased chance of blocking 


CATEGORY 4 - Faceted Orbs 
(3 types) 

Types:
* Dusk Orb, Dawn Orb, Gaia Orb 

Powers: 

Depending on the orb and what item it is socketed in, a faceted orb will grant
+1 to +2 levels to all of a specific character type's skills (except for a
couple exceptions as noted below). Unfortunately, orbs also slowly drain away
your life (-2 health regen)...so you'd better have some sort of item or gem that
has replenish health to offset this loss, or lifesteal, or something else that
will keep you from dying. 

>> Dusk Orb 
* Weapons - Assassin bonus
* Armor/helms - Sorceress bonus
* Shield - Necromancer bonus 

>> Dawn Orb 
* Weapons - Amazon bonus
* Armor/helms - Paladin bonus
* Shield - +2 to +4 to fire skills 

>> Gaia Orb 
* Weapons - 50% chance to cast lvl 6 Hand of God skill (formerly known as Fist
of the Heavens) when you are hit
* Armor/helms - Druid bonus
* Shield - Barbarian bonus 


CATEGORY 5 - Fire Blossoms 
(7 types) 

Types:
* Veridian, Amber, Scarlet, Violet, Cyan, Copper, Indigo 

Powers: 

Depending on the type of fire blossom and the item it is socketed into, it will
grant +1 to +2 all skills in a particular skill tab. The gems draw on the
character's own spiritual energy to do this; thus, each fire blossom reduces a
character's Energy by 10. 

-------------------------------------

C) NON-UPGRADEABLE GEMS, TALISMANS & GLYPHS


These items are immutable. They cannot be altered or upgraded in the Horadric
Cube. 


CATEGORY 1 - Normal Glyphs 
(5 types) 

Glyphs are intended to give bonuses, but to also come with a small penalty of
some sort 

Types:
* Hedron Glyph, Oracle Glyph, Stony Glyph, Lacquered Glyph, Agate Glyph 

>> Hedron Glyph 
Powers:
* Weapons - Percentage bonus to attack rating, Bonus to health, All resistances
reduced
* Armor/helms - Percentage increase to defenive rating, Bonus to attack rating,
Reduced health
* Shield - Percentage increase to max health, Bonus to defense, Reduced attack
rating 

>> Oracle Glyph 
Powers:
* Weapons - Ignore target's AC, Reduces character's Energy
* Armor/helms - Better chance to find magic items, Reduces character's Energy
* Shield - Better chance to find magic items (based on character level), Reduces
character's Energy 

>> Stony Glyph 
Powers:
* Weapons - Chance of crushing blow, Knockback, Reduces character's Dexterity
* Armor/helms - Character takes less damage from normal (non-magical, non-
elemental) attacks when hit, Reduces character's Dexterity
* Shield - Fastest hit recovery, Increases strength, Reduces character's
Dexterity 

>> Lacquered Glyph 
Powers:
* Weapons - Repair durability of item, Piercing attack, Reduces character's
Strength
* Armor/helms - Freeze target, Character cannot be frozen, Reduces character's
Strength
* Shield - Fastest block, Fastest run/walk, Reduces character's Strength 

>> Agate Glyph 
Powers:
For all items, this glyph will reduce requirements for the item by a tremendous
amount (50% to 75%), but will also reduce hit points (health) by 10 to 25
points.

 
CATEGORY 2 - Charmed Prisms 
(5 types) 

These are magical gemstones. Each is quite different from the rest in terms of
powers granted, but the prisms themselves look virtually identical (except in
color) 

Types:
* Blood Stone, King's Eye, Flesh Prism, Winter Shard, Autumn Flame 

>> Blood Stone 
Powers:
* Weapons - Percentage chance of deadly strike (based on character level)
* Armor/helms - Bonus to health (based on character level)
* Shield - Regenerate health 

>> King's Eye 
Powers:
* Weapons - Self-repairing, Percentage chance of crushing blow
* Armor/helms - Self-repairing, Percentage bonus to max health
* Shield - Self-repairing, Bonus to stamina regeneration 

>> Flesh Prism 
Powers:
* Weapons - Bonus to attack rating vs. undead, Bonus to vitality
* Armor/helms - Bonus to vitality (based on character level), Bonus to strength
* Shield - Absorb poison damage, Attacker takes damage 

>> Winter Shard 
Powers:
* Weapons - Cold damage, Reduce target's defense by a percentage
* Armor/helms - Cold damage, Cold resist
* Shield - Half freeze duration, Absorb cold damage 

>> Autumn Flame 
Powers:
* Weapons - Hit slows target, Fire damage
* Armor/helms - half freeze duration, Absorb fire damage
* Shield - Resist fire, Resist cold 


CATEGORY 3 - Imbued Talismans
(25 types) 

These are magical talismans of various types. Each is quite different in
appearance and function. 

Types:
* Conch Relic, Feral Relic, Elemental Relic, Infernal Relic, Forgemaster Relic,
Battlesong Relic, Rancor Circlet, Faerie Charm Ring, Carpenter's Thorns,
Cruciform of Judgment, Nazarene Cross, Cross of Life, Zakarum Cross, Tiki of
Despair, Tiki of Rage, Tiki of Scorn, Ape Totem, Badger Totem, Wraith Totem,
Succubus Kiss, Leprechuan's Clover, Vagabond Rose, Arachnid Totem, Jailer Totem 


>> Conch Relic 
Powers:
* Weapons - Hit slows target, Reduce target's defense with each hit
* Armor/helms - Fast swing speed, Fast run/walk
* Shield - Bonus to health (based on character level), Percentage increase to
defense 

>> Feral Relic 
Powers:
* Weapons - Bonus to strength (based on character level), Bonus to min/max
damage of weapon
* Armor/helms - Fast blocking speed, Bonus to stamina
* Shield - Bonus to defense rating, Bonus to dexterity (based on character
level) 

>> Elemental Relic 
Powers:
* Weapons - Bonus to vitality (based on character level), Knockback
* Armor/helms - Fast hit recovery, Damage (normal, not magical or elemental)
taken by hero is reduced
* Shield - Bonus to attack rating, Bonus to defense vs. missile attacks 

>> Infernal Relic 
Powers:
* Weapons - Bonus to energy, Bonus to attack rating vs. demons
* Armor/helms - Fast casting rate, Large light radius
* Shield - Hit blinds target, Hit may cause monster to flee 

>> Forgemaster Relic 
Powers:
* Weapons - Self-repairing, Increased durability, Increase to max damage
* Armor/helms - Self-repairing, Increased durability, Increased defense
* Shield - Self-repairing, Increased durability, Increased defense vs. missiles 

>> Battlesong Relic 
Powers:
* Weapons - Bonus to attack rating, Faster swing speed
* Armor/helms - Fast hit recovery, Percentage increase to defense (based on
character level)
* Shield - Percentage bonus to defense (based on character level), Damage
(normal, not magical or elemental) taken by hero is reduced 

>> Rancor Circlet 
Powers:
* Weapons - Increased weapon speed, Bonus to max damage
* Armor/helms - Bonus to attack rating, Bonus to defense vs. missiles
* Shield - Bonus to hit points, Increased chance of blocking 

>> Faerie Charm Ring 
Powers:
Increased chance of finding magic items (regardless of what it is socketed in) 

>> Carpenter's Thorns 
Powers:
* Weapons - Percentage of damage taken goes to replenish your mana, Deceased
strength
* Armor/helms - Percentage of damage taken goes to replenish your mana, Deceased
dexterity
* Shield - Percentage of damage taken goes to replenish your mana, Deceased
energy 

>> Cruciform of Judgment 
Powers:
* Weapons - Bonus to paladin skills, Attacker takes lightning damage
* Armor/helms - Increased damage inflicted on undead, Attacker takes lightning
damage
* Shield - Increased attack rating vs. undead, Attacker takes lightning damage 

>> Nazarene Cross 
Powers:
* Weapons - Bonus to paladin skills, Increased damage vs. demons
* Armor/helms - Increased attack rating vs. demons, Bonus to mana regeneration
* Shield - Bonus to mana, Absorb lightning damage 

>> Cross of Life 
Powers:
In all cases you will get: Regenerate health, Bonus to hit points and Mana
steal. Which power is the greatest will vary according to the item type (weapon,
helm/armor, shield) 

>> Zakarum Cross 
Powers:
Bonus to strength, vitality and energy in all cases. Which stat will be boosted
the most varies according to the item type (weapon, helm/armor, shield) 

>> Tiki of Despair 
Powers:
* Weapons - Chance of deadly strike, Chance to scare away monster
* Armor/helms - Mana after each kill, Chance to scare away monster
* Shield - Increased durability, Chance to scare away monster 

>> Tiki of Rage 
Powers:
* Weapons - Chance of crushing blow, Prevent monster heal
* Armor/helms - Chance of deadly strike, Prevent monster heal
* Shield - Reduce enemy's defense by half, Prevent monster heal 

>> Tiki of Scorn 
Powers:
Regardless of what you socket this is, you get: Hit blinds target and Reduce
enemy defense with each hit 

>> Ape Totem 
Powers:
Bonus to strength and Bonus to dexterity, regardless of what it is socketed in. 

>> Badger Totem 
Powers:
Depending on what item it is socketed in, a badger totem will provide a boost
for all skills in one of the three assassin skill tabs. In addition, you will
get an increase in one stat (dexterity in weapon, run/walk speed in helm, and
vitality in shield) 

>> Wraith Totem 
Powers:
All wraith totems provide very high life steal percentages, but at the cost of
losing having your own life drain away. Best used for high-HP melee characters
in major battles. In addition, when socketed in a weapon, you get a chance of
Deadly Strike...and when socketed in a helm or armor, you get a chance for
Crushing Blow. Shields only give you the basic mana steal/life drain combination 

>> Succubus Kiss 
Powers:
* Weapons - Manasteal, lifesteal and dexterity bonus
* Armor/helms - Chance to cast Life Tap when hit, faster mana regeneration
* Shield - Chance to cast Confuse when you hit an enemy, chance to cast Attract
on any monster that hits you

>> Leprechaun's Clover 
Powers:
Various levels of bonus to gold dropped by monsters and your chance to find
magic items, depending on whether you put it in a weapon, helm/armor, or shield.

>> Vagabond Rose 
Powers:
* Weapons - Reduced stamina drain, increased dexterity and better attack rating
* Armor/helms - Bonus to stamina, more gold dropped by monsters
* Shield - Faster run/walk, better blocking percentage and bonus to strength

>> Arachnid Totem 
Powers:
* Weapons - Slow enemies when you hit them, chance to cast Decrepify when you
hit an enemy
* Armor/helms - Resist poison, chance to cast Weaken when you hit an enemy
* Shield - Poison damage added to attacks, slow enemies when you hit them

>> Jailer Totem 
Powers:
* Weapons - Fast run/walk, chance to cast Bone Prison when you are hit
* Armor/helms - Chance to cast Bone Wall when you hit an enemy, faster run/walk
* Shield - Faster run/walk, freeze attack


CATEGORY 4 - Monster Parts
(18 types) 

These are magically enchanted organs or body parts from monsters. Powers tend to
be related at least somewhat to the nature of the monster from which the body
part was extracted. 

Ears

Types:
* Goblin Ear, Wendigo Ear, Imp Ear
 
>> Goblin Ear 
Powers:
Hit may cause monster to flee, Increased dexterity, Poison resist.
These three powers are granted no matter what kind of item the ear is socketed
in. However, the values vary.
A goblin ear in a weapon is strongest in the dexterity bonus and weakest in
poison resistance; in a helmet or armor, it is strongest in "hit causes monster
to flee" and weakest in the dexterity bonus; and in shields, it is strongest in
resist poison and weakest in causing monsters to flee. 

>> Wendigo Ear 
Powers:
Bonus to strength, Bonus to health, Reduce defense of enemy with each hit.
These three powers are granted no matter what kind of item the ear is socketed
in. However, the values vary. 
A wendigo ear in a weapon gives the best strength bonus, the second-best value
for reducing enemy defense, and the worst bonus to health. 
The ear in a helmet or armor gives the best reduce target's defense values, the
second-best health bonus, and the worst strength bonus. 
In a shield, it grants the best health bonus, the second-best strength bonus,
and the worst reduce enemy defense values. 

>> Imp Ear 
Powers:
Bonus to stamina regeneration, Bonus to attack rating vs. demons. 
These two powers are granted no matter what kind of item the ear is socketed in.
However, the values vary. 
In a weapon, the imp ear grants a high bonus to the attack rating increase but a
relatively low bonus to stamina regen. 
In helmets and armor, the ear grants "average" values to both powers. 
In shields, the ear provides a high level of stamina regen bonus but a
relatively low bonus to the attack rating vs. demons. 


Claws

Types:
* Werewolf Paw, Werebear Claw, Ki-Rin Talon 

>>Werewolf Paw 
Powers:
Increased weapon attack speed no matter what it is socketed in. Additional
bonuses are: Increased min/max damage (weapon), Increased hit points
(helm/armor), and Increased defence (shield) 

>>Werebear Claw 
Powers:
Knockback no matter what it is socketed in. Additional bonuses are: Chance of
crushing blow (weapon), Reduced poison duration (helm/armor), and Reduced
stamina drain (shield) 

>>Ki-Rin Talon 
Powers:
Chance of deadly strike no matter what it is socketed in. Additional bonuses
are: Dexterity bonus (weapon), Energy bonus (helm/armor), and Strength bonus
(shield) 


Skulls

Types:
* Blood Hawk Skull, Flayer Skull 

>>Blood Hawk Skull 
Powers:
Increased run/walk speed regardless of what it is socketed in. Additional powers
are: Slightly increased weapon speed (weapon), Reduced stamina drain
(helm/armor), and Slightly improved blocking speed (shield) 

>>Flayer Skull 
Powers:
* weapon - Piercing attack, Knockback, Chance of open wounds
* helm/armor - Very fast run/walk, Slightly improved hit recovery, Decreased
stamina
* shield - Bonus to defense vs. missiles, Slightly improved blocking speed 


Hearts

Types:
* Ghoul Heart, Vampire Heart, Troll Heart, Revenant Heart 

>>Ghoul Heart 
Powers:
* weapon - Poison damage
* helm/armor - Resist poison, Half freeze duration
* shield - Bonus to both attack rating and damage vs. undead 

>>Vampire Heart 
Powers:
* weapon - Life steal, Mana steal 
* helm/armor - Regenerate health, Decreased hit points
* shield - Cold damage and chilling effect added to attacks 

>>Troll Heart 
Powers:
Varying degrees of health regeneration and strength bonus, depending on what
kind of item it is socketed in. You also get varying degrees of reduction to
your max fire resistance, depending on the item socketed. 

>>Revenant Heart 
Powers:
* weapon - Freeze enemy, Bonus to vitality, Decrease in max poison resistance
* helm/armor - Bonus to resist poison, Bonus to vitality, Decrease in max cold
resistance
* shield - Bonus to hit points, Bonus to stamina, Decrease in dexterity 


Eyes

Types:
* Balrog Eye, Sorcerer's Eye 

>>Balrog Eye 
Powers:
* weapon - Adds fire damage, Prevents monster heal
* helm/armor - Adds poison damage to attacks
* shield - Adds fire damage, Cannot be frozen 

>>Sorcerer's Eye 
Powers:
* weapon - Bonus to mana, Slightly increased casting speed
* helm/armor - Bonus to mana regeneration, Percentage increase to mana
* shield - Percentage of damage taken goes to replenish mana 


Miscellaneous

Types:
* Wyvren Feather, Kobold Tail, Drake Horn, Basilisk Fang 

>>Wyvren Feather 
Powers:
* weapon - Adds fire and lightning damage
* helm/armor - Adds fire damage to attacks, Slow enemies upon hitting them
* shield - Adds fire damage to attacks, Absorb percentage of fire damage taken
by character 

>>Kobold Tail 
Powers:
* weapon - Bonus to both attack rating and damage vs. demons
* helm/armor - Bonus to damage vs. demons, Increased chance of finding magic
items
* shield - Increased attack rating vs. demons, Percentage increase to mana 

>>Drake Horn 
Powers:
* weapon - Adds fire damage, Chance of open wounds
* helm/armor - Bonus to resist fire, Bonus to resist poison
* shield - Absorb some fire damage, Reduced poison duration 

>>Basilisk Fang 
Powers:
* weapon - Slow enemies after hitting them
* helm/armor - Half freeze duration, Reduced damage from magic/elemental spells
* shield - Cannot be frozen
----------------------------------------------------------------
APPENDIX 3: Sorceress skills

More Details About Sorceress Skills

The sorc's three skill trees are now Affliction (mostly attack-oriented spells),
Eldritch (masteries, passives, defense and a few miscellaneous skills), and
Locus (powerful skills for attack and defense that can only be used if you have
a staff or mage sword in hand)

AFFLICTION SPELLS:

First Level:

>> Phantom Viper: A fast-flying ghostly serpent that inflict physical damage and
poison damage (all poison damage at once, NOT over time)

>> Damnation Sphere: Inflicts fire damage and has a small explosive radius

Sixth Level:

>> Plague Eels: Several writhing bolts of green energy fan out to inflict poison
damage on enemies (all at once, NOT over time like most poison spells and items)

Twelfth Level:

>> Soulreaver: A bolt of blazing energy that savages body and spirit, inflicting
physical damage, magic damage, and knocking the enemy backward

>> Arctic Lances: Essentially Frost Nova, but with different graphics and
slightly altered damage levels

Eighteenth Level:

>> Basilisk Kiss: Essentially a "poison inferno." You exhale a deadly cloud of
venom for as long as you want (or as long as your mana holds out)

>> Iceblade Guardian: A spirit engulfs you to provide you with enhanced defense,
and flings icy swords against ranged attackers

Twenty-fourth Level:

>> Soul Eclipse: A burst of energy that inflict magic damage on your opponent
and also freezes the target for a very brief moment. If you do not select a
specific target, the spell will seek the nearest enemy.

Thirtieth Level:

>> Dragon Storm: Unleashes fiery draconish spirits in a devastating wave in
front of you, to the side of you to some degree, and even a little behind you.
Because of the wide area effect and range of this spell, there is a spell timer.
You can only case this once every several seconds.

>> Doomfall: Rains down blades of energy that do magic damage and some physical
damage against those in the area of effect.

ELDRTICH SPELLS:

First Level:

>> Spiritual Focus: Essentially the same as the original Warmth, just renamed

Sixth Level:

>> Burning Strike: Enhanced version of the original Enchant Weapon skill

>> Planeshift: Teleport renamed

Twelfth Level:

>> Scorched Path: Renamed version of Blaze with somewhat different graphical
appearance.

>> Blizzard Charm: Reduces cold resistance of enemies against the spells
Iceblade Guardian, Arctic Lances, Yeti Curse and Arctic Womb

Eighteenth Level:

>> Hellfire Wall: Simply Firewall with a new name and slightly different
appearance

>> Thunderbolt: Modified version of Lighting Bolt

Twenty-fourth Level:

>> Fire Drakes: A beefed-up version of Hydra

>> Thunderbolt Mastery: Enhances damage of the spell Thunderbolt by a vast
degree

Thirtieth Level:

>> Focus Energies: Increases the damage caused by the spells Damnation Sphere,
Soulreaver, Basilisk Kiss, Hammer of God and Dragon Storm

LOCUS SPELLS:

First Level:

>> Yeti Curse: A cousin to the D1 spell "Stone Curse," this spell inflict a very
small amount of cold damage. It's main feature is to freeze the enemy in place.
This is the only offensive cold spell available to the sorceress that will truly
freeze and opponent for any significant length of time. Best used strategically
with more damaging spells, NOT as a primary attack.

>> Arctic Womb: A field of freezing energy to enhance your defense and chill
enemies who hit you.

Sixth Level:

>> Ghost Hand: Telekinesis with better damage so that it can be used as an
offensive spell as well as a way to grab items.

>> Death Crown: Enhances you defense and severely burns enemies who strike you.

Twelfth Level:

>> Tempest Wraith: A modified form of Chain Lighting that summons a storm spirit
who electrocutes multiple foes for you.

Eighteenth Level:

>> Dimensional Cocoon: Renamed version of Energy Shield

Twenty-fourth Level:

>> Lifethief: A modified version of Static Field. It is not as lethal as in the
original D2, but still effective.

>> Falling Star: An aura the rains down fireballs from the sky on your nearby
enemies.

Thirtieth Level:

>> Arcane Razors: Inflicts Lighting Damage of a very high degree, but in a very
small radius. Best used when several monsters are mobbing you or you are stuck
in hand-to-hand combat with a foe you cannot outrun.

>> Hammer of God: A modified version of Meteor that summons a frozen projectile
with a volatile, explosive core. This means the first hit again the target is
cold damage, followed by fire damage from the explosion and continued burning of
the nearby ground thereafter.
-----------------------------------------------------------------
APPENDIX 4: Anya's Runeword Clues

****SPOILER****

YOU SHOULD FIRST READ THIS CLUE IN ANYA's INTRO SPEECH AFTER RESCUING HER. IT
TAKES APPROXIMATELY 10 MINUTES FOR THE EXTRA TEXT TO SCROLL THROUGH.

Please, I spent hours writing and conceiving these darned clues; you can spend
ten minutes reading them on-screen.

However, because you may need to refer to the clues later and because it would
be cruel to make you go through the 10-minute process every time, here is Anya's
new intro text in its entirety:

You have proven yourself a true hero to 
me and my people. 

These are dark times, warrior. I hope 
you can bring an end to Baal's reign of 
destruction. 

Our Council of Elders is gone -- my 
father, Aust, among them. The one 
thing that keeps us from total despair 
is the promise of vengeance against 
Baal. 
(Suddenly, Anya's voice is in your mind) 
Hero, the magicks of Nihlathak still 
binds me to some degree. By now you 
no doubt realize there are words of 
power that can be spawned on items 
when the right runes be placed in the 
proper order in sockets. I know all 
of these words, and I wish to tell you 
of them, whether they be mighty, 
middling, or cursed ones. But as yet, 
I cannot break the compelling charm 
of that treacherous Elder and speak 
them to you. At best, I can speak to 
your mind and give you clues as to 
which runes in which order will 
make each word, based on the way of 
a warrior's weapon. I wish I could do 
more, but now I will tell you what 
I can in riddles and clues: 

The arsonist, the storm caller and the 
poisoner gather together to speak the 
Ancient's Pledge 

You find Valor when you have visceral 
strength, muscular strength, and no 
enemy can stay within reach 

Often, great Friendship forms as a result 
of blindness and lowered requirements 

The signs of Armageddon follow the 
granting of unbreakable power, the 
infliction of always-seeping wounds, 
the theft of life energies, and the 
stealing of magicks 

The cold touch of the healthy person 
who shoves you back will turn your 
world to Black 

At the edge of the Void stand four: the 
ghoul-slayer, the unshatterable one, the 
devil-hunter, and the bone-crusher 

Frozen solid, attacked with great skill, 
and armor being chipped away, you will 
know the nature of Fury 

The word Beauty shines brightly at 
the same time it eases burdens 

War costs many men their eyes, is 
fought by those who know armor's 
weak point, and requires strength 

Strip away the armor, burn the flesh, 
shock the system and envenom the foe 
to summon Holy Thunder 

Born of a weak blow made stronger, a 
strong blow made more harmful, and the 
essence of fire is the Dragon 

The word of Truth slays demonic forces 
and sends the dead back to the grave 

In the power of Blood is the theft of life 
and warring against the undead 

The runeword Water is formed by 
using three runes that capture the 
nature of this process: heat melts frozen 
ice to make frigid water. When you 
know the runes that fit that statement, 
reverse their order to make the word 

Perchance you wish to Dream? You 
need energy, you need to be able to 
force back anything, and you need 
to bear heavy loads more easily 

This is Trust: You know the enemy's 
wounds will not close, you know the 
sun will rise, and you know you can 
thwart the power of a healer 

The pronouncement of Judgment 
follows when the weapon is made 
harder, heavier, and hungry for mana 

O green-eyed Jealousy, can you not 
see that you are blinded by greed? 

To seek the Bone, know how to slay 
the living dead and boost your health 

This is Vengeance: Steal from those 
who cannot budge 

You know Treachery when you slay 
the risen dead yet yourself steal the life 
essence of the living 

The word Insight conjures thoughts of 
brightness and of shock and of heat 

To gain Authority, one must strike fear 
into hearts, possess fierce strength, 
and be a receptable of great energy 

You can send opponents fleeing or 
knock them flat...you have the Voice 

Peace is forged by highest and lowest 

The opposites ice and flame are Enigma 

To hear the Whisper is threefold: war 
with undead things, slay hellspawned 
creatures, and stand in the sunlight 

They will know Grief when their hurts 
cannot be healed, their wounds 
continue to bleed, and they cannot 
shield their eyes from the sun 

To show Penitence, you must be 
unbandaged and broken-boned 

The secret of Youth is knowing that 
the load is now lighter, having visceral 
fortitude, and gaining mental sharpness 

A burning fire, a strong push, and the 
spark of a thunderbolt create Duress 

Do you know that Wrath can blind, it 
can break mighty armor, and it can 
give you the edge in dueling? 

To exact Revenge, let armor be nothing 
before you and pry open scabs on foes 

To unleash Oblivion, cloud the vision, 
compromise the armor, and take life 
that isn't yours 

Patience is physical and mental energy 

Your foes' Last Wish will not be granted 
if you attack with deadliness, strike fear 
into their hearts, sip from their dead 
comrades' souls, and force them back 

A strong Edge more easily pierces armor 
and is tempered by a blacksmith's forge 

The bringer of Pestilence steals from 
the living, fouls the wells, and knows 
the secrets of the minions of living death 

The word of Honor comes, strangely 
enough, from the union of one of the 
thieves, the lantern bearer, the one 
who hits the hardest, he who gains 
mana from murder, and the one who 
strikes most weakly 

If you believe the Myth, you have been 
struck swiftly, been touched by Zeus' 
power, and give no mind to armor'd foes 

Tradition is as undying as a reanimated 
corpse and unchanging as the icecaps 

The King's Grace is granted when the 
thief of life, the fire-eater and the 
snow-tender stand in line to greet him 

Who would think that a Leaf is formed 
when one is killed to take their magic 
and then burned to a cinder? 

The secrets of Time are known by four: 
the magic-seeker, the juggler, the 
candle-maker, and the storm-watcher 

Law requires strength of muscle and gut 

The secret of the Lionheart is lightening 
the load, stoking the mental fires and 
strengthening the muscles 

When lightning strikes and when your 
weapon does extra damage by nicking 
the flesh, you will know Lore 

The formation of Dread begins with 
striking fear into enemies, leaving no 
shadows for them to hide in, and never 
letting them bind their wounds 

When your weapon harms just a little 
more than it should when you strike 
with full force, the sun shines on your 
face, and your enemy's armor is thinned 
before your eyes..and you know Malice 

To reach Enlightenment, expand your 
mental energies and stare into a candle 

A mighty enough Wind can chill to the 
bone, twist like an agile dancer, and 
send people running. Another runeword, 
Winter, is formed by transposing those 
last two of the three runes 

The blood is poisoned and the body 
stumbles backward...then Delirium 

The word Terror is born of a deadly 
blow and cowards running from it 

Awaken yon Beast with a double-hard 
blow of a weapon, the knocking aside 
of those who oppose you, and the 
ability to act switfly in combat 

Love is twofold: It is undying (like some 
of the rotting enemies you fight) and it 
is eternally unbreakable 

One word, Lightning, is agile and 
shocking. Its cousin, Storm, is a mirror 

The sweet sounds of the Melody sing 
true when played in front of the quick 
dueling master, the acrobat and the 
bully who pushes weaklings backward 

Temptation is a twin force. On the one 
hand, you dextrously reach for the prize. 
On the other hand, conscience pushes 
you away from it. 

The Phoenix is known of one of the 
thieves and his sightless companion 

On the Wings of Hope, your fortitude is 
boundless, your burdens are lightened, 
and your mystical reserves are boosted 

To truly Thirst, you must know the 
weakness of the zombie and its kinfolk, 
know the weakness of fresh wounds, 
and sip from the lifeblood of others 

In the midst of the Tempest, thou canst 
not see, armor is rent, and vitality is vital 

To speak an Oath requires accuracy 
and illumination 

To see great Memory, expand your 
mystic energies, increase your fortitude, 
wound enemies with a mere scratch, 
and learn to find chinks in armor 

What is Thought: agility of perception, 
radiance of intellect, and energy of mind 

If things come too easy and you are 
crushed by life, will ye Desire more? 

Obedience can be both damaging and 
fearful in its power 

To reach the Nadir, knock backward 
a person and kill him to take a sip from 
his magic wellspring 

Frozen in its shape in the sky, knocking 
the stars away from its edges, and 
striking out at demons on the surface of 
the earth...that is the Crescent Moon 

The rolling of the Thunder uncovers 
magic things, drops men to their knees, 
and smells of ozone 

To realize Lust, one must cast aside 
burdens, send other suitors running, 
and burn with desire 

Force the foe backward, give him a 
flesh-wound, and then give him a deep 
wound and you will see Radiance 

To wrap yourself in Chains of Honor, 
make your sword-arm faster, bear a 
candle, and make weapons mightier 

Certainly, it is no secret that a Brand 
brings together light and fire 

Madness sends men fleeing, pushes 
them to their knees, and strikes too 
quickly to avoid 

Weapon swung fast to armor's weak 
cast is a pair of actions that Rhyme 

Solve a Mystery with three things: your 
mental acuity and your ability to gain 
mana from two sources 

For the Darkness, combine the shining 
light with the power of blindness 

For the Daylight, enhance illumination 
with deadly damaging warrior precision 

You know the Morning is fiery and bright 

It is no wonder that you would Envy 
those with great fortitude, mental 
focus, agility, and physical strength 

O, this Pillar of Faith will make men 
stumble, stop them in their tracks, and 
give you intestinal stamina 

Behold the Flickering Flame, formed of 
fire and energy and nicking damage 

In the game of Chance, one seeks 
both golden wealth and magic trinkets 

Two words are so similar in construction: 
Praise and Prayer. One is winter's 
embrace followed by a hurricane wind's 
impact. The other is the reverse. 

If you wish to hear Silence, you must 
strike fear into the enemy, know how 
best to harm the minions of undeath, 
learn how to carry equipment with 
less strain, learn how to seek items 
of mystic power, take magical energy 
from those you slay, and even take 
such energy from those you have not 
yet killed 

In the midst of Famine, life is stolen and 
wounds fester unceasingly 

If you know how to fight demons, how 
to vanquish the undead, and have 
inner fortitude, you have great Faith 

The mightiest blow is mightier, the blade 
is venomous, the enemy burns, lightning 
strikes, and snow falls for a Call to Arms 

To know Prowess in Battle is to leave 
enemies near-dead with a single blow, 
never let them stay close, and make 
them feel even the lightest touch of 
your weapon 

Who would think Smoke would come 
from forcing back energy? 

Your burdens are light and light 
surrounds you, o child of Innocence 

When the search for riches meets an 
easing of burdens, Deception follows 

Delivering venom to foe's flesh after 
finding the chink in the armor is Stealth 

It is Prudence when a clumsy strike still 
hurts a foe, leaves him bruised even 
after teleporting, and keeps him at 
arm's length 

When wounds won't mend, and life 
drains to feed another, that is Death 

A scratch still wounds, wound-mending 
stops, and darkness flees...from Purity 

Harmony is found in power unleashed 
from dark clouds, snow from the skies, 
and lava from the earth 

Hatred springs from fear that crushes 

The harness the strength of Steel, you 
will murder for mana while light shines 

You have a Question? Then reach out 
quickly, knock aside obstacles, and 
act with damaging force 

What breeds Chaos? It seems to be a 
mixture of fearful fleeing, lightning 
striking, forceful knockback, and 
unbreakability 

In the Heart of the Oak lies a force that 
sends you running, forces you back, 
and smells of thunder 

To know Strength is to steal life and to 
take sorcerous power from the slain 

Would you like to Reason with me? Do 
you have the agility, intelligence, and 
illumination to do so? 

Bound in a prison that leaves you blind, 
freezes you still, where the bars cannot 
be broken...that breeds Despair 

Rend the flesh so that it cannot mend 
and push foes backward into Bramble 

To open the Rift, you must know 
blindness, be uncowed by shields or 
breastplates, and be a better spellcaster 

A fierce Hunger arises, awoken by one 
of the thieves, a murderer for mana, the 
other thief, and the torturer 

There lies a great Stone...three things 
you need: A wellspring of mana, a 
reservoir of stamina, and a talent 
for shouldering great weight 

To use toxin in battle, that makes foes' 
mind seeth with terror, and keeps each 
wound from closing is powerful Venom 

The dark Shadow hazes the sight, 
rusts the armor, and leaps nimbly 

Ah, it is Fortitude, gained by potential 
for stamina, a pushy personality, and 
large muscles 

Standing still and bearing the weight, 
you will see Serendipity 

When you bring together unbreakable 
character, relieve burdensome 
requirements, and boost the potential 
for magical ability, you will meet 
Destiny's Daughter 

When you see the Son of Prophecy, 
he will carry his burden with little effort, 
he will never need the blacksmith's 
hammer, and he will cast spells with 
less concern 

Ice bears both aspects of winter 

Is Gloom part of your life? Then you 
must have been crushed and blinded 

Both icy and stormy are Holy Tears 

You often see a Broken Promise when 
burdens are light and wealth is sought 

To know that the burden should not be 
so light and that you are truly sightless, 
these two things point to Humility 

Sometimes it comes easy, brings woe 
to hellspawns, and casts aside the 
gloom...it is Heaven's Will 

To find Wealth means getting more 
gold while also having nimble fingers 
and a talent for gaining mana from dead 

Feel the Breath of the Dying when 
the poisoned cloud steals life 

Making an enemy go White in the face 
means hitting them so hard they flee, 
and having boundless fortitude 

It is sometimes true that Glory is found 
in possessing wealth, being of sound 
mind, and being a shining light to all 

Starlight in this land is strange in three 
ways: It is not enough to see clearly by, 
yet it helps you see chinks in armor, 
and sends opponents running 

Destruction comes from making the 
smallest and the greatest strikes more 
deadly, while thirdly adding the light 
of truth. 

Doomsayer is the same as 
Destruction, but that the first and 
second are switched 

Of course you will feel Sorrow when 
injuries become infected, you no longer 
have the cover of darkness, and you 
no longer see clearly. Yet somehow, 
another runeword, Splendor, is made 
when the order of events is reversed 

You will feel the Hand of Justice when 
weapons' blows are doubled in power, 
the dead give up mystical power, and 
the arm is swift to strike 

The wind known as the Zephyr comes 
from lighting that pierces the armor 

The Siren's Song is fearful enough 
to make you flee, yet also make you 
feel like you can carry any weight 

You wish to have Fortune's Favor? 
Then you must strike with accuracy, 
seek magic with accuracy, and find 
treasure with accuracy 

Three runewords embody the essence 
of "forever." They are Eternity, Infinity 
and Exile. Each uses the same runes 
but in different orders. One rune is the 
lowest of all, one rune is the highest, 
and the other unlocks inner energies 

There are 21 words of special power, 
which serve specific heroes' specific 
strengths...three for each...and each 
needing four runes. Three runes are 
always the same: That which brings 
mental clarity to summon magic, that 
which illuminates the world ahead of 
you, and that which staves off the 
armorer. Between the second and third 
I have just told lie the rune that alters 
from word to word, such as follows: 
Kingslayer makes hard blows deadlier 
Knight's Vigil attacks more precisely 
Lawbringer knows that fire reveals truth 
Loyalty requires a portion of foe's life 
Nature's Kingdom blossoms from gold 
Nightfall knows demons' weakness 
Obsession is frozen in time 
Passion flees the blow 
Pattern has a swift arm 
Peril blinds what it touches 
Plague allows no cut to close 
Punishment steals sorcery via weapons 
Rain has nimble hands and feet 
Sanctuary loves the winter 
Spirit knows the making of venom 
Still Water weakens armor 
Sting inhibits healing 
Unbending Will is storm-spawned 
Victory increases weapons' lethality 
Wisdom takes magic from death 
Wonder forces everything backwards 

There are seven other special 
runewords as well, one for each kind of 
hero we have seen walk these lands. 
Each word begins with mightiness and 
the ability to carry weight evenly. The 
third rune in each varies, as follows: 
Mist sends enemies running 
Piety bears down with blinding speed 
Pride has the constitution to continue 
Principle is agile indeed 
Red is a thief 
Knowledge is energetic of mind 
Woe makes a firm hit harder still


